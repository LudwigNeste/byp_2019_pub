Beat Your Prof Presentation 2019
====

# Live version
You can find the live-version [here](https://ludwigneste.gitlab.io/byp_2019_pub).

# License

This website was made with "[reveal.js](https://github.com/hakimel/reveal.js)".

Somewhere "[MathJax](https://github.com/mathjax/MathJax)" was used.

The [Blop](https://freesound.org/s/161628/)-Sound and the [Drummroll](https://freesound.org/s/428611/)-Sound are licensed under the [creative commons 0](https://creativecommons.org/publicdomain/zero/1.0/) licence.

The [Win](https://freesound.org/s/391539/) and the [Big Win](https://freesound.org/s/456968/) Sound are licensed under the [Attribution License](https://creativecommons.org/licenses/by/3.0/).

# Hintergründe
Die [jDPG](https://de.wikipedia.org/wiki/Junge_Deutsche_Physikalische_Gesellschaft)
[Regionalgruppe Dortmund](https://www.dpg-physik.de/vereinigungen/fachuebergreifend/ak/akjdpg/rg-dortmund) 
organisiert einmal im Jahr eine Gameshow zwischen Professoren und Studierenden.

Diese Website zeigt eine rechtefreie Version der interaktiven Folien von 2019. 
Einiges wurde dadurch wesentlich abgeändert...